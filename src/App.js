import React from "react";
import { ApolloProvider } from "@apollo/client";

import Landing from "./components/layout/Landing";
import client from "./apollo/config";

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Landing defaultValue="10" />
    </ApolloProvider>
  );
};

export default App;
