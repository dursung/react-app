import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import { StorageMutations } from "./mutations/storage";
import initialState from "./state";

const cache = new InMemoryCache();

const client = new ApolloClient({
  cache,
  resolvers: {
    Mutation: {
      ...StorageMutations,
    },
  },
});

cache.writeData({ data: initialState });

export default client;
