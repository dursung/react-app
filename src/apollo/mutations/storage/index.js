import gql from "graphql-tag";

export const UPDATE_STORAGE = gql`
  mutation updateStorage($text: String!) {
    updateStorage(text: $text) @client
  }
`;

export const StorageMutations = {
  updateStorage: (_, variables, { cache }) => {
    const newValue = variables.text;
    cache.writeData({
      data: { text: newValue },
    });
    return null;
  },
};
