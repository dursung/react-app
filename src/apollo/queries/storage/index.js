import gql from "graphql-tag";
export const GET_STORAGE = gql`
  query GetStorageValue {
    text @client
  }
`;
