import React, { useState } from "react";
import InputField from "./InputField";
import isEmpty from "../../validation/is-empty";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { UPDATE_STORAGE } from "../../apollo/mutations/storage";
import { GET_STORAGE } from "../../apollo/queries/storage";

const CustomForm = ({ value = "" }) => {
  const [name, setName] = useState(value);
  const [isChange, setIsChange] = useState(false);
  const { data = {} } = useQuery(GET_STORAGE);
  const [increment] = useMutation(UPDATE_STORAGE, {
    variables: { text: name },
  });

  const onSubmit = (e) => {
    e.preventDefault();
    increment();
  };

  const onChange = (e) => {
    setName(e.target.value);
    if (isEmpty(e.target.value)) setIsChange(false);
    else setIsChange(true);
  };

  return (
    <form onSubmit={onSubmit}>
      <InputField
        placeholder="Enter a value"
        name="name"
        value={name}
        onChange={onChange}
        isChange={isChange}
      />
      <p>{data.text !== "" ? "Value is " + data.text : ""}</p>
      <input type="submit" className="btn btn-info btn-block mt-4" />
    </form>
  );
};

export default CustomForm;
