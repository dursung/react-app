import React from "react";
import CustomForm from "../common/CustomForm";

const Landing = () => {
  return (
    <div className="landing">
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <p className="lead text-center">Exercise</p>
            <CustomForm value="10" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing;
