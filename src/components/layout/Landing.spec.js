import React from "react";
import { shallow } from "enzyme";

import Landing from "./Landing";

/**
 * Factory function to create a ShallowWrapper for the App component
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = () => {
  return shallow(<Landing />);
};

describe("Landing component, if there are no text", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });

  it("renders correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
