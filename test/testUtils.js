import checkPropTypes from "check-prop-types";
import { createStore } from "redux";

import rootReducer from "../src/reducers";

/**
 * Create a testing store with imported reducers, middleware and initial state.
 * @param {object} initilState - Initial state for store.
 * @function storeFactory
 * @returns {Store} - Redux store
 */
export const storeFactory = (initialState) => {
  return createStore(rootReducer, initialState);
};

/**
 * Return ShallowWrapper containing node(s) with the given ___ value.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
 * @param {String} val - Value of ... attribute for search.
 * @returns {ShallowWrapper}
 */
export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[className="${val}"]`);
};

export const checkProps = (component, conformingProp) => {
  const propError = checkPropTypes(
    component.propTypes,
    conformingProp,
    "prop",
    component.name
  );
  expect(propError).toBeUndefined();
};
